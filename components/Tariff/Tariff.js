import React from "react";
import styles from "./Tariff.module.scss"
import TableOptions from "../TableOptions/TableOptions";


export default function Tariff() {
    return (
        <div className={styles.tariff}>
            <h2 className={styles.header}>Тарифный план</h2>
            <div className={styles.plateBlock}>
                <div className={styles.plateSmall}>
                    <div className={styles.plateSmall__main}>
                        <p className={styles.plateSmall__price}>1000 ₽</p>
                        <h3 className={styles.plateSmall__title}>БАЗОВЫЙ</h3>
                        <ul className={styles.plateSmall__description}>
                            <li className={styles.plateSmall__benefit}>все возможности сервиса Доски</li>
                            <li className={styles.plateSmall__benefit}>поддержка в чате</li>
                        </ul>
                    </div>
                    <button className={styles.plateSmall__purchase}>КУПИТЬ</button>
                </div>
                <div className={styles.plateBig}>
                    <div className={styles.plateBig__main}>
                        <p className={styles.plateBig__price}>1500 ₽</p>
                        <h3 className={styles.plateBig__title}>PRO</h3>
                        <ul className={styles.plateBig__description}>
                            <li className={styles.plateBig__benefit}>все возможности сервиса Доски</li>
                            <li className={styles.plateBig__benefit}>поддержка в чате</li>
                            <li className={styles.plateBig__benefit}>30 минут консультаций в день</li>
                        </ul>
                    </div>
                    <button className={styles.plateBig__purchase}>КУПИТЬ</button>
                </div>
                <div className={styles.plateSmall}>
                    <div className={styles.plateSmall__main}>
                        <p className={styles.plateSmall__price}>3000 ₽</p>
                        <h3 className={styles.plateSmall__title}>ВСЕ ВКЛЮЧЕНО</h3>
                        <ul className={styles.plateSmall__description}>
                            <li className={styles.plateSmall__benefit}>все возможности сервиса Доски</li>
                            <li className={styles.plateSmall__benefit}>поддержка в чате</li>
                            <li className={styles.plateSmall__benefit}>30 минут консультаций в день</li>
                            <li className={styles.plateSmall__benefit}>консультант в течение рабочего дня.</li>
                        </ul>
                    </div>
                    <button className={styles.plateSmall__purchase}>КУПИТЬ</button>
                </div>
            </div>
            <TableOptions/>
        </div>
    )
}