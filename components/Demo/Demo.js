import React from "react";
import styles from "./Demo.module.scss"


export default function Demo() {
    return (
        <div className={styles.demo}>
            <h2 className={styles.header}>Получить демо-версию - легко!</h2>
            <form className={styles.form}>
                <div className={styles.inputBlock}>
                    <label className={styles.label}>ВАШ рабочий E-mail</label>
                    <input className={styles.field} type="text" placeholder="denis@company.com"/>
                </div>
                <button className={styles.regButton}>РЕГИСТРИРУЙСЯ</button>
            </form>
            <div className={styles.altReg}>
                <p className={styles.altReg__caption}>Или зарегистрироваться через</p>
                <img className={styles.altReg__logo} alt="Google" src="/Google.png"/>
                <img className={styles.altReg__logo} alt="Facebook" src="/Facebook.png"/>
            </div>
            <p className={styles.signIn}>Уже есть аккаунт? <a href="#" className={styles.signLink}>Войти</a></p>
        </div>
    )
}