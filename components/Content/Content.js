import React from "react";
import styles from "./Content.module.scss";
import Scope from "../Scope/Scope";
import Auto from "../Auto/Auto";
import Analysis from "../Analysis/Analysis";
import Clarify from "../Clarify/Clarify";
import Questions from "../Questions/Questions";
import Tariff from "../Tariff/Tariff";
import Demo from "../Demo/Demo";
import RegBlock from "../RegBlock/RegBlock";

export default function Content() {
    return (
        <section className={styles.mainContent}>
            <Scope/>
            <RegBlock/>
            <Auto/>
            <Analysis/>
            <Clarify/>
            <Questions/>
            <Tariff/>
            <Demo/>
        </section>
    )
}