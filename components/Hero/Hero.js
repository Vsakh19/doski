import React from "react";
import styles from "./Hero.module.scss"


export default function Hero() {
    return (
        <section className={styles.hero}>
            <h1 className={styles.header}>«Доски» – новый черный в управленческом учете</h1>
            <h2 className={styles.subHeader}>Ты можешь видеть все составляющие бизнеса на одном листе: внутреннюю
                отчетность и бухучет. Реальные и виртуальные транзакции – все, что
                нужно для правильного принятия решений.</h2>
            <button className={styles.demo}>получить демо-доступ</button>
            <img className={styles.pic} alt='' src='/hero.png'/>
        </section>
    )
}