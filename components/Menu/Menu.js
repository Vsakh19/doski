import React from "react";
import styles from './Menu.module.scss'


export default function Menu() {
    return (
        <nav className={styles.navBar}>
            <h2 className={styles.navBar__header}>Доски</h2>
            <ul className={styles.navBar__list}>
                <li className={styles.navBar__column}><a href='#' className={styles.navBar__link}>Возможности</a></li>
                <li className={styles.navBar__column}><a href='#' className={styles.navBar__link}>Цены</a></li>
                <li className={styles.navBar__column}><a href='#' className={styles.navBar__link}>Плюсы</a></li>
                <li className={styles.navBar__column}><a href='#' className={styles.navBar__link}>Ответы</a></li>
            </ul>
            <button className={styles.navBar__auth}>Регистрируйся</button>
        </nav>
    )
}