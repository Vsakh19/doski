import React from "react";
import styles from "./Auto.module.scss";


export default function Auto() {
    return (
        <div className ={styles.auto}>
            <div className={styles.line}/>
            <h2 className={styles.header}>Автоматизация учета</h2>
            <p className={styles.description}>Экономь время! Освободи голову для принятия решений.
                Все, что надо - автоматизировано.</p>
            <div className={styles.partners}>
                <img className={styles.partnersPic} alt="" src="/1C.png"/>
                <img className={styles.partnersPic} alt="" src="/VTB.png"/>
                <img className={styles.partnersPic} alt="" src="/Bitrix24.png"/>
                <img className={styles.partnersPic} alt="" src="/otkrytie.png"/>
                <img className={styles.partnersPic} alt="" src="/amocrm.png"/>
                <img className={styles.partnersPic} alt="" src="/Rosbank.png"/>
                <img className={styles.partnersPic} alt="" src="/yclients.png"/>
            </div>
        </div>
    )
}