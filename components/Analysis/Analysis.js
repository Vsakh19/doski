import React from "react";
import styles from "./Analysis.module.scss"


export default function Analysis() {
    return (
        <div className={styles.analysis}>
            <div className={styles.main}>
                <div className={styles.line}/>
                <h2 className={styles.header}>Аналитика на одном экране</h2>
                <p className={styles.description}>Правильные решения – принимаются когда данные валидны и своевременны.
                    Просто создай свои графики и получай ясную картину бизнеса. Для еще более простого понимания мы
                    предлагаем свой шаблон данных на дашборде. Твой бизнес – твоя прозрачность</p>
                <img className={styles.graph} alt="graph" src="/graph.png"/>
            </div>
        </div>
    )
}