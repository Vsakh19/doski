import React from "react";
import styles from "./TableOptions.module.scss"



export default function TableOptions() {
    return (
        <table className={styles.table}>
            <thead>
                <tr className={styles.titleRow}>
                    <td className={`${styles.field} ${styles.empty}`}/>
                    <td className={`${styles.field} ${styles.basic}`}>БАЗОВЫЙ</td>
                    <td className={`${styles.field} ${styles.pro}`}>PRO</td>
                    <td className={`${styles.field} ${styles.allInc}`}>ВСЕ ВКЛЮЧЕНО</td>
                </tr>
            </thead>
            <tbody>
                <tr className={styles.titleRow}>
                    <td className={`${styles.field} ${styles.option}`}>все возможности сервиса</td>
                    <td className={`${styles.field} ${styles.checker}`}>✓</td>
                    <td className={`${styles.field} ${styles.checker}`}>✓</td>
                    <td className={`${styles.field} ${styles.checker}`}>✓</td>
                </tr>
                <tr className={styles.titleRow}>
                    <td className={`${styles.field} ${styles.option}`}>поддержка в чате</td>
                    <td className={`${styles.field} ${styles.checker}`}>✓</td>
                    <td className={`${styles.field} ${styles.checker}`}>✓</td>
                    <td className={`${styles.field} ${styles.checker}`}>✓</td>
                </tr>
                <tr className={styles.titleRow}>
                    <td className={`${styles.field} ${styles.option}`}>30 минут консультаций в день</td>
                    <td className={`${styles.field} ${styles.checker}`}/>
                    <td className={`${styles.field} ${styles.checker}`}>✓</td>
                    <td className={`${styles.field} ${styles.checker}`}>✓</td>
                </tr>
                <tr className={styles.titleRow}>
                    <td className={`${styles.field} ${styles.option}`}>консультант в течение рабочего дня</td>
                    <td className={`${styles.field} ${styles.checker}`}/>
                    <td className={`${styles.field} ${styles.checker}`}/>
                    <td className={`${styles.field} ${styles.checker}`}>✓</td>
                </tr>
                <tr className={styles.titleRow}>
                    <td className={`${styles.field} ${styles.option}`}>сколько юр.лиц может пользоваться</td>
                    <td className={`${styles.field} ${styles.uno}`}>1</td>
                    <td className={`${styles.field} ${styles.desc} ${styles.short}`}>до 5</td>
                    <td className={`${styles.field} ${styles.desc} ${styles.short}`}>от 5</td>
                </tr>
                <tr className={styles.titleRow}>
                    <td className={`${styles.field} ${styles.option}`}>виртуальный счет</td>
                    <td className={`${styles.field} ${styles.uno}`}>1</td>
                    <td className={`${styles.field} ${styles.uno}`}>1</td>
                    <td className={`${styles.field} ${styles.uno}`}>1</td>
                </tr>
                <tr className={styles.titleRow}>
                    <td className={`${styles.field} ${styles.option}`}>количество предприятий</td>
                    <td className={`${styles.field} ${styles.uno}`}>1</td>
                    <td className={`${styles.field} ${styles.desc}`}>до 5 с разными вариантами налогообложения</td>
                    <td className={`${styles.field} ${styles.desc}`}>от 5 с разными вариантами налогообложения</td>
                </tr>
                <tr className={styles.titleRow}>
                    <td className={`${styles.field} ${styles.option}`}>контрагенты</td>
                    <td className={`${styles.field} ${styles.uno}`}>1</td>
                    <td className={`${styles.field} ${styles.desc}`}>неограниченное количество</td>
                    <td className={`${styles.field} ${styles.desc}`}>неограниченное количество</td>
                </tr>
                <tr className={styles.titleRow}>
                    <td className={`${styles.field} ${styles.option}`}>количество людей, одновременно работающих с сервером</td>
                    <td className={`${styles.field} ${styles.uno}`}>1</td>
                    <td className={`${styles.field} ${styles.desc}`}>до 10 с разным уровнем доступа</td>
                    <td className={`${styles.field} ${styles.desc}`}>до 30 с разным уровнем доступа</td>
                </tr>
                <tr className={styles.titleRow}>
                    <td className={`${styles.field} ${styles.option}`}>дополнительные интеграции</td>
                    <td className={`${styles.field} ${styles.uno}`}>1</td>
                    <td className={`${styles.field} ${styles.desc}`}>до 3 новых подключений</td>
                    <td className={`${styles.field} ${styles.desc}`}>неограниченное кол-во подключений</td>
                </tr>
            </tbody>
        </table>
    )
}