import React from "react";
import styles from "./Footer.module.scss"

export default function Footer() {
    return (
        <section className={styles.footer}>
            <h2 className={styles.logo}>Доски</h2>
            <ul className={styles.linkList}>
                <li className={styles.linkRow}><a className={styles.link} href="#">Главная</a></li>
                <li className={styles.linkRow}><a className={styles.link} href="#">Возможности</a></li>
                <li className={styles.linkRow}><a className={styles.link} href="#">Цены</a></li>
                <li className={styles.linkRow}><a className={styles.link} href="#">Плюсы</a></li>
                <li className={styles.linkRow}><a className={styles.link} href="#">Ответы</a></li>
                <li className={styles.linkRow}><a className={styles.link} href="#">Регистрация</a></li>
            </ul>
            <div className={styles.contact}>
                <div className={styles.contact__type}>
                    <h3 className={styles.contact__header}>ОСТАЛИСЬ ВОПРОСЫ? мы вам перезвоним</h3>
                    <form className={styles.form}>
                        <input type="text" placeholder="Ваш телефон" className={styles.form__input}/>
                        <button className={styles.form__submit}>Отправить</button>
                    </form>
                </div>
                <div className={styles.contact__type}>
                    <h3 className={styles.contact__header}>Наши соц.сети</h3>
                    <div className={styles.socialBlock}>
                        <a href="#"><img className={styles.social} alt="Google" src="/Google.png"/></a>
                        <a href="#"><img className={styles.social} alt="Be" src="/Behance.png"/></a>
                        <a href="#"><img className={styles.social} alt="Facebook" src="/Facebook.png"/></a>
                    </div>
                </div>
            </div>
        </section>
    )
}