import React from "react";
import styles from "./Questions.module.scss"


export default function Questions() {
    return (
        <div className={styles.questions}>
            {/*<div className={styles.bg}/>*/}
            <div className={styles.main}>
                <h2 className={styles.header}>Остались вопросы – возможно у нас уже есть на них ответы</h2>
                <div className={styles.sample}>
                    <p className={styles.sample__text}>Подойдет ли мне ваш сервис?</p>
                    <p className={styles.sample__expand}>+</p>
                </div>
                <div className={styles.sample}>
                    <p className={styles.sample__text}>Можно ли работать с телефона/планшета?</p>
                    <p className={styles.sample__expand}>+</p>
                </div>
                <div className={styles.sample}>
                    <p className={styles.sample__text}>Насколько защищены мои данные?</p>
                    <p className={styles.sample__expand}>+</p>
                </div>
                <div className={styles.sample}>
                    <p className={styles.sample__text}>Мой бизнес растет,  ваш сервис будет полезен, после
                        привлечения инвесторов?</p>
                    <p className={styles.sample__expand}>+</p>
                </div>
                <button className={styles.regButton}>РЕГИСТРИРУЙСЯ</button>
            </div>
        </div>
    )
}