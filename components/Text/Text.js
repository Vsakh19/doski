import React from "react";
import styles from "./Text.module.scss"


export default function Text(props) {
    return (
        <div className={styles.textContent}>
            <div className={styles.line}/>
            <h2 className={styles.header}>{props.header}</h2>
            <p className={styles.text}>{props.data}</p>
            <a href={props.link} className={styles.link}>Подробнее</a>
        </div>
    )
}