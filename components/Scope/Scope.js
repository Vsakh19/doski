import React from "react";
import styles from "./Scope.module.scss"
import TextBlock from "../TextBlock/TextBlock";


export default function Scope() {
    return (
        <div className={styles.scope}>
            <div className={styles.line}/>
            <h2 className={styles.header}>возможности</h2>
            <p className={styles.description}>Больше не надо тратить часы, чтобы разделить виртуальные и реальные
                транзакции и обязательства. Все возможности Business Intelligence на одном экране, с простым и понятным
                интерфейсом. Удобный ручной ввод и автоматизация данных из других сервисов. Мы внедрили все,
                чем хотим пользоваться сами.</p>
            <img className={styles.pic} alt='Expenses' src='/expenses.jpg'/>
            <TextBlock/>
        </div>
    )
}