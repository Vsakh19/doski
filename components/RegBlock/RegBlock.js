import React from "react";
import styles from "./RegBlock.module.scss";


export default function RegBlock() {
    return (
        <div className={styles.regBlock}>
            <h2 className={styles.regBlock__header}>Удобный и понятный управленческий учет!  Просто!</h2>
            <button className={styles.regBlock__button}>регистрируйся</button>
        </div>
    )
}