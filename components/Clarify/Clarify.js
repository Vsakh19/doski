import React from "react";
import styles from "./Clarify.module.scss";


export default function Clarify() {
    return (
        <div className={styles.clarify}>
                <div className={styles.line}/>
                <h2 className={styles.header}>Вноси Ясность</h2>
                <p className={styles.description}>Скажи нет тетрадкам и экселевским табличкам в секретных папках.
                    Держи свои записи в порядке при себе в облаке. Применяй фильтры и получай
                    реальное состояние дел. </p>
                <img className={styles.graph} alt="graph expenses" src="/graphExp.png"/>
        </div>
    )
}