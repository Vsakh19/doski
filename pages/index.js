import React from "react";
import styles from '../styles/Main.module.scss'
import Menu from '../components/Menu/Menu'
import Hero from '../components/Hero/Hero'
import Content from '../components/Content/Content'
import Footer from "../components/Footer/Footer";

export default function Main() {
  return (
      <div className={styles.page}>
        <Menu/>
        <Hero/>
        <Content/>
        <Footer/>
      </div>
  )
}